const ResponseLogger = () => async (ctx, next) => {
    console.log(ctx.params);
    await next();
    const rt = ctx.response.get('X-Response-Time');
    console.log(`${ctx.method} ${ctx.url} - ${rt}`);
};

export default ResponseLogger;
