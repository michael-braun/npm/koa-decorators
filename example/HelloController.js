import {
    Route,
    AcceptVerbs,
    HttpGet,
    Middleware,
    decorateMiddleware,
} from '../lib/index';

import ResponseLogger from './middlewares/ResponseLogger';
import ResponseTime from './middlewares/ResponseTime';

const ResponseLoggerDecorator = () => decorateMiddleware(ResponseLogger());
const ResponseTimeDecorator = decorateMiddleware(ResponseTime);

@Route('/hello/:test')
@Route('/hi2')
@ResponseLoggerDecorator()
export default class HelloController {
    @Route('/test')
    @AcceptVerbs('get')
    @ResponseTimeDecorator
    static async Get(ctx) {
        ctx.body = 'Hello World!';
    }

    @HttpGet()
    static async GetValue(ctx) {
        ctx.body = 'Hello Get!';
    }

    @HttpGet('/ipsum/:ipsi')
    @Middleware(ResponseTime)
    static async GetIpsum(ctx) {
        ctx.body = 'Hello Lorem Ipsum!';
    }
}
