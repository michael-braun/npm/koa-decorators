import Koa from 'koa';
import Router from 'koa-router';
import HelloController from './HelloController';
import { createRoutes } from '../lib';

const app = new Koa();
const router = new Router();

createRoutes(router, HelloController);

app
    .use(router.routes())
    .use(router.allowedMethods());

app.listen(process.env.PORT || 8080);
