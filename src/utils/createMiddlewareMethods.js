export default function createMiddlewareMethods() {
    const middlewares = [];

    return {
        setter: (...middleware) => {
            middlewares.unshift(...middleware);
        },
        getter: () => middlewares,
    };
}
