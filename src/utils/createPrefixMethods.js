export default function createPrefixMethods() {
    const prefixes = [];

    return {
        setter: (prefix) => {
            prefixes.push(prefix);
        },
        getter: () => prefixes,
    };
}
