export default class StaticMethod {
    kind = 'method';

    key = '';

    placement = 'static';

    descriptor = {
        writable: false,
        configurable: false,
        enumerable: false,
        value: null,
    };

    constructor(name, callback) {
        this.key = name;
        this.descriptor.value = callback;
    }
}
