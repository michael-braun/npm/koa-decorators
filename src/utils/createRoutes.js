/* eslint-disable no-underscore-dangle */
import Router from 'koa-router';
import NoMethodError from '../errors/NoMethodError';
import UnsupportedMethodError from '../errors/UnsupportedMethodError';

export default function createRoutes(rootRouter, controller) {
    const properties = Object.getOwnPropertyNames(controller);

    let prefixes = null;
    if (!controller._getPrefix?.() || controller._getPrefix().length === 0) {
        prefixes = [''];
    } else {
        prefixes = controller._getPrefix();
    }

    const classMiddlewares = controller._getMiddlewares && controller._getMiddlewares();

    const routers = prefixes.map(() => new Router());

    properties.forEach((property) => {
        if (!controller[property]?._routed) {
            return;
        }

        const {
            _routes: routes,
            _verbs: verbs,
            _middlewares: routeMiddlewares,
        } = controller[property];

        if (!verbs || verbs.length === 0) {
            throw new NoMethodError(property);
        }

        const middlewares = [];
        if (classMiddlewares) {
            middlewares.push(...classMiddlewares);
        }

        if (routeMiddlewares) {
            middlewares.push(...routeMiddlewares);
        }

        ((routes && routes.length) ? routes : ['/']).forEach((route) => {
            routers.forEach((router) => {
                verbs.forEach((verb) => {
                    if (!router[verb]) {
                        throw new UnsupportedMethodError(verb);
                    }

                    router[verb](route, ...middlewares, controller[property]);
                });
            });
        });
    });

    routers.forEach((router, index) => {
        rootRouter.use(prefixes[index], router.routes(), router.allowedMethods());
    });
}
