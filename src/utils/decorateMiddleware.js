import Middleware from '../decorators/Middleware';

export default function decorateMiddleware(middleware) {
    return Middleware(middleware);
}
