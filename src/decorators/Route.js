/* eslint-disable no-underscore-dangle */

import createPrefixMethods from '../utils/createPrefixMethods';
import StaticMethod from '../utils/StaticMethod';
import UnsupportedContextError from '../errors/UnsupportedContextError';

export default function Route(route) {
    return (target) => {
        if (target?.kind === 'method' && target.placement === 'static') {
            if (!target.descriptor.value._routes) {
                /* eslint-disable-next-line no-param-reassign */
                target.descriptor.value._routes = [];
            }

            target.descriptor.value._routes.push(route);

            if (!target.descriptor.value._routed) {
                /* eslint-disable-next-line no-param-reassign */
                target.descriptor.value._routed = true;
            }

            return target;
        }

        if (target?.kind === 'class') {
            let index = target.elements.findIndex(e => e.kind === 'method' && e.placement === 'static' && e.key === '_setPrefix');

            if (index === -1) {
                const { getter, setter } = createPrefixMethods();

                target.elements.push(new StaticMethod('_setPrefix', setter));
                index = target.elements.length - 1;

                target.elements.push(new StaticMethod('_getPrefix', getter));
            }

            target.elements[index].descriptor.value(route);

            return target;
        }

        throw new UnsupportedContextError('Route', 'class', 'static-method');
    };
}
