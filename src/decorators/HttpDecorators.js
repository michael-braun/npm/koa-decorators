import UnsupportedContextError from '../errors/UnsupportedContextError';
import AcceptVerbs from './AcceptVerbs';
import Route from './Route';

export function createDecorator(name, verb) {
    return route => (target) => {
        const { kind } = target;

        if (kind !== 'method') {
            throw new UnsupportedContextError(name, 'static-method');
        }

        let newTarget = AcceptVerbs(verb)(target);

        if (route) {
            newTarget = Route(route)(newTarget);
        }

        return newTarget;
    };
}

export const HttpGet = createDecorator('HttpGet', 'GET');
export const HttpPost = createDecorator('HttpPost', 'POST');
export const HttpPut = createDecorator('HttpPut', 'PUT');
export const HttpPatch = createDecorator('HttpPatch', 'PATCH');
export const HttpDelete = createDecorator('HttpDelete', 'DELETE');
