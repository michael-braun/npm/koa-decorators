/* eslint-disable no-underscore-dangle */
import methods from 'methods';
import isString from 'lodash.isstring';

import UnsupportedContextError from '../errors/UnsupportedContextError';
import MethodAlreadySetError from '../errors/MethodAlreadySetError';
import NoMethodError from '../errors/NoMethodError';
import UnsupportedMethodError from '../errors/UnsupportedMethodError';

export default function AcceptVerbs(...verbs) {
    return (target) => {
        if (target?.kind !== 'method' || target.placement !== 'static') {
            throw new UnsupportedContextError('AcceptVerbs', 'static-method');
        }

        if (target.descriptor.value._verbs) {
            throw new MethodAlreadySetError();
        }

        if (!target.descriptor.value._routed) {
            /* eslint-disable-next-line no-param-reassign */
            target.descriptor.value._routed = true;
        }

        if (!verbs || verbs.length === 0) {
            throw new NoMethodError(target.key);
        }

        /* eslint-disable-next-line no-param-reassign */
        target.descriptor.value._verbs = verbs.map((verb) => {
            if (!isString(verb) || !methods.includes(verb.toLowerCase())) {
                throw new UnsupportedMethodError(verb);
            }

            return verb.toLowerCase();
        });

        return target;
    };
}
