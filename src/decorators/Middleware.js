/* eslint-disable no-underscore-dangle */
import UnsupportedContextError from '../errors/UnsupportedContextError';
import StaticMethod from '../utils/StaticMethod';
import createMiddlewareMethods from '../utils/createMiddlewareMethods';

export default function Middleware(...middlewares) {
    return (target) => {
        if (target?.kind === 'method' && target.placement === 'static') {
            if (!target.descriptor.value._middlewares) {
                /* eslint-disable-next-line no-param-reassign */
                target.descriptor.value._middlewares = [];
            }

            target.descriptor.value._middlewares.unshift(...middlewares);

            return target;
        }

        if (target?.kind === 'class') {
            let index = target.elements.findIndex(e => e.kind === 'method' && e.placement === 'static' && e.key === '_addMiddleware');

            if (index === -1) {
                const { getter, setter } = createMiddlewareMethods();

                target.elements.push(new StaticMethod('_addMiddleware', setter));
                index = target.elements.length - 1;

                target.elements.push(new StaticMethod('_getMiddlewares', getter));
            }

            target.elements[index].descriptor.value(...middlewares);

            return target;
        }

        throw new UnsupportedContextError('Middleware', 'class', 'static-method');
    };
}
