export default class UnsupportedMethodError extends Error {
    constructor(method) {
        super(`method "${method}" not supported`);

        Error.captureStackTrace(this, UnsupportedMethodError);
    }
}
