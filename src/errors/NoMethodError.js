export default class NoMethodError extends Error {
    constructor(propertyName) {
        super(`no method specified for ${propertyName}`);

        Error.captureStackTrace(this, NoMethodError);
    }
}
