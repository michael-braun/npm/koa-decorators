export default class MethodAlreadySetError extends Error {
    constructor() {
        super('request method already set');

        Error.captureStackTrace(this, MethodAlreadySetError);
    }
}
