export default class UnsupportedContextError extends Error {
    constructor(decoratorName, ...allowedTypes) {
        super(`decorator @${decoratorName} only allowed on ${allowedTypes.join(',')}`);

        Error.captureStackTrace(this, UnsupportedContextError);
    }
}
