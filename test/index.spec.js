import { expect } from 'chai';

import * as KoaDecoratorsExports from '../src/index';
import Route from '../src/decorators/Route';
import AcceptVerbs from '../src/decorators/AcceptVerbs';
import Middleware from '../src/decorators/Middleware';
import createRoutes from '../src/utils/createRoutes';
import decorateMiddleware from '../src/utils/decorateMiddleware';
import {
    HttpGet,
    HttpPost,
    HttpPut,
    HttpPatch,
    HttpDelete,
} from '../src/decorators/HttpDecorators';

describe('index', () => {
    describe('exports', () => {
        it('Route', () => {
            expect(KoaDecoratorsExports).to.have.property('Route');
            expect(KoaDecoratorsExports.Route).to.be.equal(Route);
        });

        it('AcceptVerbs', () => {
            expect(KoaDecoratorsExports).to.have.property('AcceptVerbs');
            expect(KoaDecoratorsExports.AcceptVerbs).to.be.equal(AcceptVerbs);
        });

        it('Middleware', () => {
            expect(KoaDecoratorsExports).to.have.property('Middleware');
            expect(KoaDecoratorsExports.Middleware).to.be.equal(Middleware);
        });

        it('createRoutes', () => {
            expect(KoaDecoratorsExports).to.have.property('createRoutes');
            expect(KoaDecoratorsExports.createRoutes).to.be.equal(createRoutes);
        });

        it('decorateMiddleware', () => {
            expect(KoaDecoratorsExports).to.have.property('decorateMiddleware');
            expect(KoaDecoratorsExports.decorateMiddleware).to.be.equal(decorateMiddleware);
        });

        it('HttpVerb', () => {
            expect(KoaDecoratorsExports).to.have.property('HttpGet');
            expect(KoaDecoratorsExports.HttpGet).to.be.equal(HttpGet);

            expect(KoaDecoratorsExports).to.have.property('HttpPost');
            expect(KoaDecoratorsExports.HttpPost).to.be.equal(HttpPost);

            expect(KoaDecoratorsExports).to.have.property('HttpPut');
            expect(KoaDecoratorsExports.HttpPut).to.be.equal(HttpPut);

            expect(KoaDecoratorsExports).to.have.property('HttpPatch');
            expect(KoaDecoratorsExports.HttpPatch).to.be.equal(HttpPatch);

            expect(KoaDecoratorsExports).to.have.property('HttpDelete');
            expect(KoaDecoratorsExports.HttpDelete).to.be.equal(HttpDelete);
        });
    });
});
