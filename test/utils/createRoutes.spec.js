import { expect } from 'chai';
import sinon from 'sinon';

import { createRoutes } from '../../src';
import NoMethodError from '../../src/errors/NoMethodError';
import UnsupportedMethodError from '../../src/errors/UnsupportedMethodError';

describe('createRoutes', () => {
    it('export', () => {
        expect(createRoutes).to.be.a('function');
    });

    let router;

    let controller;

    beforeEach(() => {
        router = {
            use: sinon.stub(),
        };

        controller = {
            _getPrefix: () => {},
            _getMiddlewares: () => {},
        };
    });

    it('no-method error', () => {
        controller.test = Object.assign(() => null, {
            _routed: true,
        });

        expect(() => createRoutes(router, controller)).to.throw(NoMethodError);
    });

    it('method only', () => {
        controller.test = Object.assign(() => null, {
            _routed: true,
            _verbs: ['get', 'post'],
        });

        expect(() => createRoutes(router, controller)).to.not.throw();
        expect(router.use.firstCall).to.be.not.null;
        expect(router.use.firstCall.args[0]).to.be.equal('');
        expect(router.use.firstCall.args[1].router.stack).to.have.property('length').to.be.equal(2);

        expect(router.use.firstCall.args[1].router.stack[0].path).to.be.equal('/');
        expect(router.use.firstCall.args[1].router.stack[0].methods).to.be.eql(['HEAD', 'GET']);

        expect(router.use.firstCall.args[1].router.stack[1].path).to.be.equal('/');
        expect(router.use.firstCall.args[1].router.stack[1].methods).to.be.eql(['POST']);
    });

    it('unsupported method', () => {
        controller.test = Object.assign(() => null, {
            _routed: true,
            _verbs: ['unsupported'],
        });

        expect(() => createRoutes(router, controller)).to.throw(UnsupportedMethodError);
    });

    it('custom prefix', () => {
        controller.test = Object.assign(() => null, {
            _routed: true,
            _verbs: ['get', 'post'],
        });
        controller._getPrefix = () => ['/test1', '/test2']; /* eslint-disable-line no-underscore-dangle */

        expect(() => createRoutes(router, controller)).to.not.throw();
        expect(router.use.firstCall).to.be.not.null;
        expect(router.use.firstCall.args[0]).to.be.equal('/test1');
        expect(router.use.firstCall.args[1].router.stack).to.have.property('length').to.be.equal(2);
        expect(router.use.firstCall.args[1].router.stack[0].path).to.be.equal('/');
        expect(router.use.firstCall.args[1].router.stack[0].methods).to.be.eql(['HEAD', 'GET']);
        expect(router.use.firstCall.args[1].router.stack[1].path).to.be.equal('/');
        expect(router.use.firstCall.args[1].router.stack[1].methods).to.be.eql(['POST']);

        expect(router.use.secondCall).to.be.not.null;
        expect(router.use.secondCall.args[0]).to.be.equal('/test2');
        expect(router.use.secondCall.args[1].router.stack).to.have.property('length').to.be.equal(2);
        expect(router.use.secondCall.args[1].router.stack[0].path).to.be.equal('/');
        expect(router.use.secondCall.args[1].router.stack[0].methods).to.be.eql(['HEAD', 'GET']);
        expect(router.use.secondCall.args[1].router.stack[1].path).to.be.equal('/');
        expect(router.use.secondCall.args[1].router.stack[1].methods).to.be.eql(['POST']);
    });

    it('class middlewares', () => {
        const middleware1 = () => null;
        const middleware2 = () => null;

        controller.test = Object.assign(() => null, {
            _routed: true,
            _verbs: ['get', 'post'],
        });

        /* eslint-disable-next-line no-underscore-dangle */
        controller._getMiddlewares = () => [middleware1, middleware2];

        expect(() => createRoutes(router, controller)).to.not.throw();

        expect(router.use.called).to.be.true;
        expect(router.use.firstCall.args[1].router.stack[0].stack)
            .to.include.members([middleware1, middleware2]);
        expect(router.use.firstCall.args[1].router.stack[1].stack)
            .to.include.members([middleware1, middleware2]);
    });

    it('route middlewares', () => {
        const middleware1 = () => null;
        const middleware2 = () => null;

        controller.test1 = Object.assign(() => null, {
            _routed: true,
            _verbs: ['get'],
            _middlewares: [middleware1, middleware2],
        });
        controller.test2 = Object.assign(() => null, {
            _routed: true,
            _verbs: ['get'],
            _middlewares: [middleware1],
        });

        expect(() => createRoutes(router, controller)).to.not.throw();

        expect(router.use.called).to.be.true;
        expect(router.use.firstCall.args[1].router.stack[0].stack)
            .to.include.members([middleware1, middleware2]);
        expect(router.use.firstCall.args[1].router.stack[1].stack)
            .to.include.members([middleware1]);
    });
});
