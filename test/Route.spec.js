import { expect } from 'chai';

import UnsupportedContextError from '../src/errors/UnsupportedContextError';
import { Route } from '../src';

describe('Route', () => {
    let fakeTarget = null;

    beforeEach(() => {
        fakeTarget = {
            kind: 'method',
            placement: 'static',
            descriptor: {
                value: {},
            },
            elements: [],
        };
    });

    describe('usage', () => {
        it('invalid use', () => {
            expect(() => Route()(null)).to.throw(UnsupportedContextError);
            expect(() => Route()({})).to.throw(UnsupportedContextError);
        });

        describe('correct use, invalid type', () => {
            it('non-static method', () => {
                expect(() => {
                    class Test {
                        @Route()
                        test() { /* eslint-disable-line class-methods-use-this */
                        }
                    }

                    return Test;
                }).to.throw(UnsupportedContextError);
            });

            it('static property', () => {
                expect(() => {
                    class Test {
                        @Route('/test')
                        static test = () => {
                        }
                    }

                    return Test;
                }).to.throw(UnsupportedContextError);
            });
        });

        describe('correct use', () => {
            it('static method', () => {
                expect(() => {
                    class Test {
                        @Route('/test')
                        static test() {
                        }
                    }

                    return Test;
                }).to.not.throw();
            });

            it('class', () => {
                expect(() => {
                    @Route()
                    class Test {
                    }

                    return Test;
                }).to.not.throw();
            });
        });
    });

    describe('set data', () => {
        it('on method', () => {
            const stubMiddleware = {};
            Route(stubMiddleware)(fakeTarget);
            expect(fakeTarget.descriptor.value).to.have.property('_routes').to.be.eql([stubMiddleware]);
        });

        describe('on class', () => {
            it('create methods', () => {
                fakeTarget.kind = 'class';

                const stubMiddleware = {};
                Route(stubMiddleware)(fakeTarget);
                expect(fakeTarget.elements).to.have.property('length').to.be.equal(2);

                expect(fakeTarget.elements[0]).to.have.property('kind').to.be.equal('method');
                expect(fakeTarget.elements[0]).to.have.property('key').to.be.equal('_setPrefix');
                expect(fakeTarget.elements[0]).to.have.property('placement').to.be.equal('static');

                expect(fakeTarget.elements[1]).to.have.property('kind').to.be.equal('method');
                expect(fakeTarget.elements[1]).to.have.property('key').to.be.equal('_getPrefix');
                expect(fakeTarget.elements[1]).to.have.property('placement').to.be.equal('static');
            });

            it('set data', () => {
                fakeTarget.kind = 'class';

                const prefix1 = '/test1';
                const prefix2 = '/test2';
                Route(prefix1)(fakeTarget);
                Route(prefix2)(fakeTarget);

                const middlewares = fakeTarget.elements.find(e => e.key === '_getPrefix');
                expect(middlewares.descriptor.value()).to.be.eql([prefix1, prefix2]);
            });
        });
    });
});
