import { expect } from 'chai';
import { createDecorator } from '../src';
import UnsupportedContextError from '../src/errors/UnsupportedContextError';

describe('HttpDecorators', () => {
    let fakeTarget = null;

    beforeEach(() => {
        fakeTarget = {
            kind: 'method',
            placement: 'static',
            descriptor: {
                value: {},
            },
        };
    });

    it('error', () => {
        fakeTarget.kind = 'class';
        expect(() => createDecorator('Wrong decorator', 'GET')('/')(fakeTarget))
            .to.throw(UnsupportedContextError)
            .to.have.property('name').to.be.equal(new UnsupportedContextError('Wrong decorator', 'static-method').name);
    });
});
