import { expect } from 'chai';

import UnsupportedContextError from '../src/errors/UnsupportedContextError';
import { Middleware } from '../src';

describe('Middleware', () => {
    let fakeTarget = null;

    beforeEach(() => {
        fakeTarget = {
            kind: 'method',
            placement: 'static',
            descriptor: {
                value: {},
            },
            elements: [],
        };
    });

    describe('usage', () => {
        it('invalid use', () => {
            expect(() => Middleware()(null)).to.throw(UnsupportedContextError);
            expect(() => Middleware()({})).to.throw(UnsupportedContextError);
        });

        describe('correct use, invalid type', () => {
            it('non-static method', () => {
                expect(() => {
                    class Test {
                        @Middleware()
                        test() { /* eslint-disable-line class-methods-use-this */
                        }
                    }

                    return Test;
                }).to.throw(UnsupportedContextError);
            });

            it('static property', () => {
                expect(() => {
                    class Test {
                        @Middleware('GET')
                        static test = () => {
                        }
                    }

                    return Test;
                }).to.throw(UnsupportedContextError);
            });
        });

        describe('correct use', () => {
            it('static method', () => {
                expect(() => {
                    class Test {
                        @Middleware('GET')
                        static test() {
                        }
                    }

                    return Test;
                }).to.not.throw();
            });

            it('class', () => {
                expect(() => {
                    @Middleware()
                    class Test {
                    }

                    return Test;
                }).to.not.throw();
            });
        });
    });

    describe('set data', () => {
        it('on method', () => {
            const stubMiddleware = {};
            Middleware(stubMiddleware)(fakeTarget);
            expect(fakeTarget.descriptor.value).to.have.property('_middlewares').to.be.eql([stubMiddleware]);
        });

        describe('on class', () => {
            it('create methods', () => {
                fakeTarget.kind = 'class';

                const stubMiddleware = {};
                Middleware(stubMiddleware)(fakeTarget);
                expect(fakeTarget.elements).to.have.property('length').to.be.equal(2);

                expect(fakeTarget.elements[0]).to.have.property('kind').to.be.equal('method');
                expect(fakeTarget.elements[0]).to.have.property('key').to.be.equal('_addMiddleware');
                expect(fakeTarget.elements[0]).to.have.property('placement').to.be.equal('static');

                expect(fakeTarget.elements[1]).to.have.property('kind').to.be.equal('method');
                expect(fakeTarget.elements[1]).to.have.property('key').to.be.equal('_getMiddlewares');
                expect(fakeTarget.elements[1]).to.have.property('placement').to.be.equal('static');
            });

            it('set data', () => {
                fakeTarget.kind = 'class';

                const stubMiddleware1 = {};
                const stubMiddleware2 = {};
                Middleware(stubMiddleware1)(fakeTarget);
                Middleware(stubMiddleware2)(fakeTarget);

                const middlewares = fakeTarget.elements.find(e => e.key === '_getMiddlewares');
                expect(middlewares.descriptor.value()).to.be.eql([
                    stubMiddleware1,
                    stubMiddleware2,
                ]);
            });
        });
    });
});
