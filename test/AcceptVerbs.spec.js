import { expect } from 'chai';
import { AcceptVerbs } from '../src';
import UnsupportedContextError from '../src/errors/UnsupportedContextError';
import NoMethodError from '../src/errors/NoMethodError';
import UnsupportedMethodError from '../src/errors/UnsupportedMethodError';
import MethodAlreadySetError from '../src/errors/MethodAlreadySetError';

describe('AcceptVerbs', () => {
    let fakeTarget = null;

    beforeEach(() => {
        fakeTarget = {
            kind: 'method',
            placement: 'static',
            descriptor: {
                value: {},
            },
        };
    });

    describe('usage', () => {
        it('invalid use', () => {
            expect(() => AcceptVerbs()(() => null)).to.throw(UnsupportedContextError);
            expect(() => AcceptVerbs()({})).to.throw(UnsupportedContextError);
        });

        describe('correct use, invalid type', () => {
            it('class', () => {
                expect(() => {
                    @AcceptVerbs()
                    class Test {
                    }

                    return Test;
                }).to.throw(UnsupportedContextError);
            });

            it('non-static method', () => {
                expect(() => {
                    class Test {
                        @AcceptVerbs()
                        test() { /* eslint-disable-line class-methods-use-this */
                        }
                    }

                    return Test;
                }).to.throw(UnsupportedContextError);
            });

            it('static property', () => {
                expect(() => {
                    class Test {
                        @AcceptVerbs('GET')
                        static test = () => {
                        }
                    }

                    return Test;
                }).to.throw(UnsupportedContextError);
            });
        });

        it('correct use', () => {
            expect(() => {
                class Test {
                    @AcceptVerbs('GET')
                    static test() {
                    }
                }

                return Test;
            }).to.not.throw();
        });
    });

    describe('parameter', () => {
        it('no parameter', () => {
            expect(() => AcceptVerbs()(fakeTarget)).to.throw(NoMethodError);
        });

        it('invalid parameter', () => {
            expect(() => AcceptVerbs({})(fakeTarget)).to.throw(UnsupportedMethodError);
        });

        it('invalid between valid parameter', () => {
            expect(() => AcceptVerbs('GET', {}, 'POST')(fakeTarget)).to.throw(UnsupportedMethodError);
        });
    });

    it('set data', () => {
        const target = AcceptVerbs('GET', 'POST')(fakeTarget);
        expect(target.descriptor.value).to.have.property('_routed').to.be.equal(true);
        expect(target.descriptor.value).to.have.property('_verbs').to.be.eql(['get', 'post']);
    });

    it('method already set', () => {
        fakeTarget.descriptor.value._verbs = []; /* eslint-disable-line no-underscore-dangle */
        expect(() => AcceptVerbs('GET', 'POST')(fakeTarget)).to.throw(MethodAlreadySetError);
    });
});
